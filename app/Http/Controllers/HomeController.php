<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('index');
    }
    
    public function about()
    {
        return view('about', [
            "fname" => "Utut",
            "lname" => "Arta",
            "age" => "21",
            "nationality" => "Indonesia",
            "freelance" => "Available",
            "address" => "Singaraja, Bali",
            "phone" => "+6287863035506",
            "email" => "utut@undikhsa.ac.id",
            "instagram" => "ututarta",
            "langages" => "Indonesia, English"
        ]);
    }

    public function portfolio()
    {
        return view('portfolio');
    }

    public function contact()
    {
        return view('contact');
    }

    public function blog()
    {
        return view('blog');
    }

    public function blogpost()
    {
        return view('blog-post');
    }
}
